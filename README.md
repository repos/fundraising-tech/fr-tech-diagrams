# fr-tech-diagrams

Repository for Fundraising Tech diagrams. 


#### UML Diagrams

This repo contains text definition files which can used to generate diagrams over at https://plantuml.com/

To generate a diagram from a text definition, head over to the Plant UML running server [here](https://www.plantuml.com/plantuml/uml/SyfFKj2rKt3CoKnELR1Io4ZDoSa70000). Paste the text definition into the text area and click the submit button. 

#### Non-UML Diagrams

The "Fundraising_Tech_Architecture_Diagram.jpg" was created in google docs, source available: https://docs.google.com/drawings/d/1QnZYYNAtnituAs9IsSmgPiHAC1EKto9WAWmeh4Dpi-g/edit

### PHPStorm users
Install this extension! https://plugins.jetbrains.com/plugin/7017-plantuml-integration
